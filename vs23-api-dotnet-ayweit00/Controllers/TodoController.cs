using Data;
using Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace vs23_api_dotnet_ayweit00.Controllers;


[ApiController]
[Route("api/[controller]")]
public class TodosController : ControllerBase
{
  private readonly TodoContext _context;

  public TodosController(TodoContext context)
  {
    _context = context;
  }

  // GET: api/todos
  [HttpGet]
  public async Task<ActionResult<IEnumerable<Todo>>> GetTodos()
  {
    return await _context.Todos.ToListAsync();
  }


  // GET: api/todos/5
  [HttpGet("{id}")]
  public async Task<ActionResult<Todo>> GetTodo(int id)
  {
    var todo = await _context.Todos.FindAsync(id);

    if (todo == null)
    {
      return NotFound();
    }

    return todo;
  }


  // POST: api/todos
  [HttpPost]
  public async Task<ActionResult<Todo>> PostTodo(Todo todo)
  {
    _context.Todos.Add(todo);
    await _context.SaveChangesAsync();

    return CreatedAtAction(nameof(GetTodo), new { id = todo.Id }, todo);
  }

  // dummy method to test the connection
  [HttpGet("hello")]
  public string Test()
  {
    return "Hello World!";
  }
}