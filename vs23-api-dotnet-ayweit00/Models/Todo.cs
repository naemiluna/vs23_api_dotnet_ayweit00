using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table("todos")]
    public class Todo
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("todo")]
        public string? Name { get; set; }

        [Column("priority")]
        public int Priority { get; set; }
    }
}
